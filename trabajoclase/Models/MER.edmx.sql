
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/15/2015 13:30:05
-- Generated from EDMX file: C:\Users\DAVID\Nueva carpeta\trabajoclase\trabajoclase\Models\MER.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [master];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PersonasSet'
CREATE TABLE [dbo].[PersonasSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre_persona] nvarchar(max)  NOT NULL,
    [Apellido_persona] nvarchar(max)  NOT NULL,
    [Email_persona] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UsuariosSet'
CREATE TABLE [dbo].[UsuariosSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre_usuario] nvarchar(max)  NOT NULL,
    [Contraseña_usuario] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'PersonasSet'
ALTER TABLE [dbo].[PersonasSet]
ADD CONSTRAINT [PK_PersonasSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UsuariosSet'
ALTER TABLE [dbo].[UsuariosSet]
ADD CONSTRAINT [PK_UsuariosSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------