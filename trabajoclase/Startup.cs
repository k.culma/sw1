﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(trabajoclase.Startup))]
namespace trabajoclase
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
